#include <stdio.h>    // Standard input/output definitions
#include <stdlib.h>
#include <string.h>   // String function definitions
#include <unistd.h>   // para usleep()
#include <getopt.h>
#include <math.h>

#include "arduino-serial-lib.h"

float calculateSD(float data[]);

void error(char* msg)
{
    fprintf(stderr, "%s\n",msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
	int fd = -1;
	int baudrate = 9600;  // default
    int buff[10];
    char* tem = 't';
    char* hum = 'h';
    char* on = '1';
    char* off = '0';
    float num[12];
	fd = serialport_init("/dev/ttyACM0", baudrate);

	if( fd==-1 )
	{
		error("couldn't open port");
		return -1;
	}
    
    serialport_flush(fd);
    while (0==0){
        write(fd, on, 1);
        write(fd, tem, 1);
        write(fd, hum, 1);
        sleep(5);
        printf("%d", read(fd, tem, 1));
        printf("%d", read(fd, hum, 1));
        write(fd, off, 1);
    }
    
    char a = '1';
    char ap = '0';
    write(fd, &a, 1);
    
    //sleep(2);
    write(fd, &ap, 1);
    
    char b = 't';
    printf("%d", read(fd, tem, 1));
	close( fd );
	return 0;	
}



/* Ejemplo para calcular desviacion estandar y media */
float calculateSD(float data[])
{
    float sum = 0.0, mean, standardDeviation = 0.0;

    int i;

    for(i = 0; i < 10; ++i)
    {
        sum += data[i];
    }

    mean = sum/10;

    for(i = 0; i < 10; ++i)
        standardDeviation += pow(data[i] - mean, 2);

    return sqrt(standardDeviation / 10);
}

